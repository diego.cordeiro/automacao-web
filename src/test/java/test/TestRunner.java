package test;

import org.junit.runner.RunWith;
import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = "src/test/java/aFeatures",
		plugin = "pretty",
		snippets = SnippetType.CAMELCASE,
		glue = {""}, 
		monochrome = true, 
		dryRun = false, 
		strict = true,
		tags = { "@acessaSiteDiegoCordeiro"}
		)

@Test(priority = 11)
public class TestRunner{

}
