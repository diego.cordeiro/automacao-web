package Setups;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.Status;

import Utils.ReportUtils;
import Utils.seleniumUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestRule {

	protected static WebDriver driver;
	public static String nomeCenario;
	
	seleniumUtils seleniumutils = new seleniumUtils();
	
	@Before
	public void beforeCenario(Scenario cenario){
		ReportUtils.criarReport(cenario);
		String pathProjeto = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", pathProjeto + "/drivers/chromedriver.exe");
		ReportUtils.logMensagem(Status.INFO, "Iniciando Teste.");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		nomeCenario = cenario.getName();
		ReportUtils.logMensagem(Status.INFO, "Executando Cenário: " + nomeCenario);
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static String getNomeCenario() {
		return nomeCenario;
	}

	@After
	public void afterCenario(Scenario cenario) {
		ReportUtils.logMensagem(Status.INFO, "Finalizando Instâncias", seleniumUtils.getScreenshotReport());
		ReportUtils.atualizaReport(cenario);
		seleniumUtils.sleep(1000);
		driver.quit();
	}
}
